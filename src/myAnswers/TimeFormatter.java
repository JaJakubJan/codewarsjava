package myAnswers;

import java.util.*;
import java.util.stream.Collectors;

public class TimeFormatter {
    public static String formatDuration(int seconds) {
        return seconds == 0 ? "now" :
                Arrays.stream(
                        new String[]{
                                formatTime("year",  (seconds / 31536000)),
                                formatTime("day",   (seconds / 86400)%365),
                                formatTime("hour",  (seconds / 3600)%24),
                                formatTime("minute",(seconds / 60)%60),
                                formatTime("second",seconds%60)})
                        .filter(e -> e != "")
                        .collect(Collectors.joining(", "))
                        .replaceAll(", (?!.+,)", " and ");
        // X (?! Y) -> matches X only if X is not followed by Y
        // .+ X -> any amount of X character
    }
    private static String formatTime(String s, int time){
        return time == 0 ? "" : time + " " + s + (time == 1 ? "" : "s");
    }

}