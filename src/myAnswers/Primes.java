package myAnswers;

import java.util.BitSet;
import java.util.stream.IntStream;

public class Primes {
    private static final int LIMIT = 40_000_000;
    private static final BitSet IS_PRIME = getIsPrimeBitSetFrom(LIMIT);

    public static IntStream stream() {
        return IntStream.iterate(0, i -> i + 1).filter(IS_PRIME::get);
    }
    private static BitSet getIsPrimeBitSetFrom(int limit) {
        BitSet bitSet = new BitSet(limit);
        bitSet.set(0, false);
        bitSet.set(1, false);
        bitSet.set(2, limit, true);
        for (int i = 2; (long)i * i < limit; ++i)
            if (bitSet.get(i)) {
                int j = i;
                int x = 2;
                while (j < limit) {
                    j = i * x;
                    bitSet.set(j, false);
                    ++x;
                }
            }
        return bitSet;
    }
}