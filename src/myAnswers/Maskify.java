package myAnswers;

public class Maskify {
    public static String maskify(String str) {
        StringBuilder result = new StringBuilder();
        String lastFour = str.length() > 4 ? str.substring(str.length() - 4) : str;
        result.append("#".repeat(Math.max(0, str.length() - 4)));
        return result.append(lastFour).toString();
    }
}
