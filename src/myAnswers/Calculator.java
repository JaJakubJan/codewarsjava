package myAnswers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Calculator {
    public static Double evaluate(String expression) {
        return calculate(Stream.of(expression
                .split(" ")).collect(Collectors.toList()));
    }
    private static double calculate(List<String> elements){
        for(int i = elements.size() - 1 ; i >= 0 ; i--)
            if (elements.get(i).compareTo("(") == 0)
                for (int j = i; j < elements.size(); j++)
                    if (elements.get(j).compareTo(")") == 0){
                        if (i + 2 == j) {
                            elements.remove(j);
                            elements.remove(i);
                            return calculate(elements);
                        }
                        calculate(elements.subList(i + 1, j));
                        return calculate(elements);
                    }
        for(int i = 0 ; i < elements.size() ; i++){
            if(elements.get(i).compareTo("*") == 0) {
                count(elements, i,
                        Double.parseDouble(elements.get(i - 1)) * Double.parseDouble(elements.get(i + 1)));
                return calculate(elements);
            }else if(elements.get(i).compareTo("/") == 0) {
                count(elements, i,
                        Double.parseDouble(elements.get(i - 1)) / Double.parseDouble(elements.get(i + 1)));
                return calculate(elements);
            }
        }
        for(int i = 0 ; i < elements.size() ; i++){
            if(elements.get(i).compareTo("+") == 0) {
                count(elements, i,
                        Double.parseDouble(elements.get(i - 1)) + Double.parseDouble(elements.get(i + 1)));
                return calculate(elements);
            }else if(elements.get(i).compareTo("-") == 0) {
                count(elements, i,
                        Double.parseDouble(elements.get(i - 1)) - Double.parseDouble(elements.get(i + 1)));
                return calculate(elements);
            }
        }
        if(elements.size() > 1)
            return calculate(elements);
        return Double.parseDouble(elements.get(0));
    }
    private static double count(List<String> elements, int index, double outcome){
        elements.remove(index + 1);
        elements.set(index, String.valueOf(outcome));
        elements.remove(index - 1);
        return outcome;
    }
}
