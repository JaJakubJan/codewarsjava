package myAnswers;

import java.util.Collections;
import java.util.stream.Collectors;

public class DescendingOrder {
    public static int sortDesc(final int num) {
        return Integer.parseInt(Integer.toString(num)
                .chars()
                .mapToObj(i -> String.valueOf(Character.getNumericValue(i)))
                .sorted(Collections.reverseOrder())
                .collect(Collectors.joining()));
    }
}