package myAnswers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import static java. lang. Math. pow;

public class SumDigPower {
    public static List<Long> sumDigPow(long a, long b) {
        return LongStream.range(a, b + 1)
                .filter(SumDigPower::isEureka)
                .boxed()
                .collect(Collectors.toList());
    }
    private static boolean isEureka(long a){
        long acc = 0L;
        int[] chars = Long.toString(a).chars()
                .toArray();
        for(int i = 0 ; i < chars.length ; i++)
            acc += pow(chars[i] - '0', i + 1);
        return acc == a;
    }
}
