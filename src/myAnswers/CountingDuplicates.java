package myAnswers;

import java.util.Set;
import java.util.TreeSet;

public class CountingDuplicates {
    public static int duplicateCount(String text) {
        Set<Integer> duplicateSigns = new TreeSet<>();
        int[] signs = text.toLowerCase()
                .chars()
                .sorted()
                .toArray();
        for(int i = 1 ; i < signs.length ; i++)
            if(signs[i] == signs[i - 1])
                duplicateSigns.add(signs[i]);
        return duplicateSigns.size();
    }
}