package myAnswers;

public class MatrixDeterminant {
    public static int determinant(int[][] matrix) {
        if(matrix.length == 1) return matrix[0][0];
        if(matrix.length == 2) return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
        int result = 0;
        for(int i = 0 ; i < matrix.length ; i++)
            result += (i % 2 == 0 ? 1 : -1) * matrix[0][i] * determinant(getSubMatrixFrom(matrix, i));
        return result;
    }
    private static int[][] getSubMatrixFrom(int[][] matrix, int column){
        int[][] subMatrix = new int[matrix.length - 1][matrix.length - 1];
        for(int i = 1 ; i < matrix.length ; i++)
            for(int j = 0 ; j < matrix[i].length ; j++)
                if(j < column) subMatrix[i-1][j] = matrix[i][j];
                else if(j > column) subMatrix[i-1][j-1] = matrix[i][j];
        return subMatrix;
    }
}
