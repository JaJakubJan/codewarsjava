package myAnswers;

import java.util.ArrayList;

public class Warrior {
    private int experience = 100;
    private ArrayList<String> achievements = new ArrayList<>();
    private String[] ranks = new String[]{"Pushover", "Novice", "Fighter", "Warrior", "Veteran", "Sage",
            "Elite", "Conqueror", "Champion", "Master", "Greatest"};

    public int experience(){ return Math.min(experience, 10000); }
    public int level(){ return Math.min(experience / 100, 100); }
    public String rank() { return ranks[level()/10]; }
    public ArrayList<String> achievements(){ return achievements; }

    public String training(String description, int exp, int minLvl ){
        if(level() < minLvl)
            return "Not strong enough";
        experience += exp;
        achievements.add(description);
        return description;
    }
    public String battle(int opponentLvl){
        if(opponentLvl < 1 || opponentLvl > 100)
            return "Invalid level";
        if(opponentLvl / 10 > level() / 10 && opponentLvl - level() >= 5 )
            return "You've been defeated";
        if(level() - opponentLvl >= 2 )
            return "Easy fight";
        if(level() - opponentLvl >= 0 ){
            experience += (opponentLvl - level() + 2) * 5;
            return "A good fight"; }
        experience += 20 * (opponentLvl - level()) * (opponentLvl - level());
        return "An intense fight";
    }

}
