package myAnswers;

import java.util.Arrays;
import java.util.stream.IntStream;

public class ConwayLife {
    public static int[][] getGeneration(int[][] cells, int generations) {
        for(int i = 0 ; i < generations ; i++)
            cells = getCellsWithoutDeadBorders(getNextGeneration(cells));
        return cells;
    }

    public static int[][] getNextGeneration(int[][] previous){
        int[][] increasedPrevious = getCellsIncreasedInBorders(previous);
        int[][] next = Arrays.stream(increasedPrevious)
                .map(int[]::clone).toArray(int[][]::new);
        for(int i = 0 ; i < increasedPrevious.length ; i++)
            for(int j = 0 ; j < increasedPrevious[i].length ; j++){
                int liveNeighborsCellsNumber = getLiveNeighborsFromCell(increasedPrevious, i, j);
                if(liveNeighborsCellsNumber > 3 || liveNeighborsCellsNumber < 2)
                    next[i][j] = 0;
                else if(liveNeighborsCellsNumber == 3)
                    next[i][j] = 1;
            }
        return getCellsWithoutDeadBorders(next);
    }

    private static int getLiveNeighborsFromCell(int[][] cells, int x, int y){
        int liveNeighborsCellsNumber = 0;
        for(int i = Math.max(0, x - 1) ; i < Math.min(cells.length, x + 2) ; i++)
            for(int j = Math.max(0, y - 1) ; j < Math.min(cells[i].length, y + 2) ; j++)
                if(!(i == x && j == y ))
                    liveNeighborsCellsNumber += cells[i][j];
        return liveNeighborsCellsNumber;
    }

    private static int[][] getCellsIncreasedInBorders(int[][] cells){
        int[][] increasedCells = new int[cells.length + 2][cells[0].length + 2];
        for(int i = 0 ; i < cells.length ; i++)
            for(int j = 0 ; j < cells[i].length ; j++)
                increasedCells[i+1][j+1] = cells[i][j];
        return increasedCells;
    }

    private static int[][] getCellsWithoutDeadBorders(int[][] cells){
        if(IntStream.of(cells[0]).sum() == 0){
            int[][] cuttedCells = new int[cells.length - 1][cells[0].length];
            for(int i = 1 ; i < cells.length ; i++)
                System.arraycopy(cells[i], 0, cuttedCells[i - 1], 0, cells[i].length);
            cells = cuttedCells;
        }
        if(IntStream.of(cells[cells.length - 1]).sum() == 0){
            int[][] cuttedCells = new int[cells.length - 1][cells[0].length];
            for(int i = 0 ; i < cells.length - 1 ; i++)
                System.arraycopy(cells[i], 0, cuttedCells[i], 0, cells[i].length);
            cells = cuttedCells;
        }
        if(IntStream.of(getColumnFrom(cells, 0)).sum() == 0){
            int[][] cuttedCells = new int[cells.length][cells[0].length - 1];
            for(int i = 0 ; i < cells.length ; i++)
                for(int j = 1 ; j < cells[0].length ; j++)
                    cuttedCells[i][j-1] = cells[i][j];
            cells = cuttedCells;
        }
        if(IntStream.of(getColumnFrom(cells, cells[0].length - 1)).sum() == 0){
            int[][] cuttedCells = new int[cells.length][cells[0].length - 1];
            for(int i = 0 ; i < cells.length ; i++)
                for(int j = 0 ; j < cells[0].length - 1 ; j++)
                    cuttedCells[i][j] = cells[i][j];
            cells = cuttedCells;
        }
        return cells;
    }

    private static int[] getColumnFrom(int[][] matrix, int idx){
        int[] column = new int[matrix.length];
        for(int i = 0 ; i < matrix.length ; i++)
            column[i] = matrix[i][idx];
        return column;
    }
}
