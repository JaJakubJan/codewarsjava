package myAnswers;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class EncryptionAlternatingSplit {
    public static String encrypt(final String text, final int n) {
        if( n < 1 || text == null || text.isEmpty())
            return text;
        String encryptedText = text;
        encryptedText = (getEverySecCharFrom(encryptedText, 1) + getEverySecCharFrom(text, 0));
        encryptedText = encrypt(encryptedText, n - 1);
        return encryptedText;
    }
    private static String getEverySecCharFrom(final String text, int start){
        AtomicInteger idx = new AtomicInteger();
        return text.chars()
                .mapToObj(c -> String.valueOf((char) c))
                .filter(el -> idx.getAndIncrement() % 2 == start)
                .collect(Collectors.joining());
    }
    public static String decrypt(final String encryptedText, final int n) {
        if( n < 1 || encryptedText == null || encryptedText.isEmpty())
            return encryptedText;
        StringBuilder text = new StringBuilder();
        char[] first = encryptedText.substring(0, encryptedText.length() / 2).toCharArray();
        char[] second = encryptedText.substring(encryptedText.length() / 2).toCharArray();
        for(int i = 0 ; i < encryptedText.length() / 2; i++){
            text.append(second[i]);
            text.append(first[i]);
        }
        if(encryptedText.length() % 2 == 1)
            text.append(second[encryptedText.length() / 2]);
        text = new StringBuilder(decrypt(text.toString(), n - 1));
        return text.toString();
    }
}
