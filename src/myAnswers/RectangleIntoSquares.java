package myAnswers;

import java.util.ArrayList;
import java.util.List;

public class RectangleIntoSquares {
    public static List<Integer> sqInRect(int lng, int wdth) {
        return helper(lng, wdth, new ArrayList<Integer>());
    }
    private static List<Integer> helper(int lng, int wdth, ArrayList<Integer> list){
        if(lng > wdth){
            list.add(wdth);
            helper(lng - wdth, wdth, list);
        } else if(wdth > lng){
            list.add(lng);
            helper(lng, wdth - lng, list);
        } else
            list.add(lng);
        return list.size() == 1 ? null : list;
    }
}
