package myAnswers;

import java.util.ArrayList;
import java.util.List;

public class Decompose {

    public static String decompose(long n) {
        List<Long> sequence = getSequenceFrom(n, n * n);
        if(sequence == null) return null;
        sequence.remove(sequence.size() - 1);
        return sequence.stream().mapToLong(el -> el)
                .mapToObj(Long::toString)
                .reduce("", (acc, el) -> acc + " " + el)
                .substring(1);
    }

    private static List<Long> getSequenceFrom(long n, long rest){
        if(rest == 0){
            List<Long> list = new ArrayList<>();
            list.add(n);
            return list;
        }
        for(long i = n - 1 ; i > 0; i--){
            if((rest - i * i) >= 0){
                List<Long> list = getSequenceFrom(i, (rest - i * i));
                if(list != null){
                    list.add(n);
                    return list;
                }
            }
        }
        return null;
    }
}