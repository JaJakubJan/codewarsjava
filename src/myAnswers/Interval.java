package myAnswers;

import java.util.Comparator;
import java.util.stream.Stream;

public class Interval {
    public static int sumIntervals(int[][] intervals) {
        if(intervals == null) return 0;
        return Stream.of(intervals)
                .sorted(Comparator.comparingInt(el -> el[0]))
                .reduce(new int[]{Integer.MIN_VALUE, 0} ,(acc, el) ->
                    f(el, acc[0], acc[1])
                )[1];
    }
    private static int[] f(int[] interval, int lastIdx, int numberOfElements){
        for(int i = Math.max(interval[0], lastIdx) ; i < interval[1] ; i++)
            numberOfElements++;
        return new int[]{Math.max(interval[1], lastIdx), numberOfElements};
    }
}
