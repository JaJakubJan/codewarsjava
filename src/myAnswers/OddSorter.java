package myAnswers;

import java.util.Iterator;
import java.util.stream.IntStream;

public class OddSorter {
    public static int[] sortArray(int[] array) {
        Iterator<Integer> sortedOdds = IntStream.of(array)
                .filter(el -> el % 2 == 1)
                .sorted()
                .iterator();
        return IntStream.of(array)
                .map(el -> el % 2 == 1 ? sortedOdds.next(): el)
                .toArray();
    }
}
