package myAnswers;

import java.util.*;

public class Snail {
    private static int[][] matrix;
    public static int[] snail(int[][] matrix) {
        Snail.matrix = matrix;
        ArrayList<int[]> res = new ArrayList<>();
        for(int i = 0 ; i < matrix.length ; i++ ){
            res.add(takeRow(Math.max(i - 1, i), i, i, false));
            res.add(takeColumn(matrix[0].length - i - 1, i + 1, i, false));
            res.add(takeRow(matrix.length - i - 1, i, i + 1, true));
            res.add(takeColumn(i, i + 1, i + 1, true));
            if(res.get(res.size() - 1).length == 0)
                return getArrayFromListOfArrays(res);
        }
        return null;
    }
    private static int[] getArrayFromListOfArrays(ArrayList<int[]> list){
        ArrayList<Integer> tempList = new ArrayList<>();
        list.stream().filter(el -> el.length != 0)
                .forEach(el -> Arrays.stream(el).forEach(tempList::add));
        int[] res = new int[tempList.size()];
        for(int i = 0 ; i < res.length ; i++)
            res[i] = tempList.get(i);
        return res;
    }
    private static int[] takeColumn(int columnNumber, int start, int end, boolean isReverse){
        if(matrix.length - start - end < 0) return new int[]{};
        int[] column = new int[matrix.length - start - end];
        for(int i = start ; i < matrix.length - end ; i++ )
            column[i - start] = matrix[i][columnNumber];
        if(isReverse)
            return reverse(column);
        return column;
    }
    private static int[] takeRow(int rowNumber, int start, int end, boolean isReverse){
        if(matrix[rowNumber].length - start - end < 0) return new int[]{};
        int[] row = new int[matrix[rowNumber].length - start - end];
        for(int i = start ; i < matrix[rowNumber].length - end ; i++ )
            row[i - start] = matrix[rowNumber][i];
        if(isReverse)
            return reverse(row);
        return row;
    }
    private static int[] reverse(int[] array){
        for(int i=0; i<array.length/2; i++){
            int temp = array[i];
            array[i] = array[array.length -i -1];
            array[array.length -i -1] = temp;
        }
        return array;
    }
}