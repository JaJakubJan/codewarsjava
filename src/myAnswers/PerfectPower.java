package myAnswers;

public class PerfectPower {
    public static int[] isPerfectPower(int n) {
        final double MARGIN = 0.0000001;
        final int MAX_POW = 30;
        for(int i = 2 ; i < Math.min(MAX_POW, n) ; i++){
            double root = Math.pow(n, 1.0 / (i * 1.0));
            if(Math.pow((int) (root + MARGIN), i * 1.0) == n)
                return new int[]{ (int) (root + MARGIN), i};
        }
        return null;
    }
}
