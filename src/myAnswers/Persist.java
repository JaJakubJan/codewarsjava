package myAnswers;

public class Persist {
    public static int persistence(long n) {
       return helper(n, 0);
    }
    private static int helper(long n, int times){
        if(n < 10)
            return times;
        return helper(Long.toString(n)
                .chars()
                .mapToObj(i -> Integer.parseInt(String.valueOf(Character.getNumericValue(i))))
                .reduce(1, (acc, el) -> acc * el), ++times);
    }
}
