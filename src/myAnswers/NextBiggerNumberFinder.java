package myAnswers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class NextBiggerNumberFinder {
    public static long nextBiggerNumber(long n) {
        int[] digits = Long.toString(n)
                .chars().map(c -> c - '0').toArray();
        ArrayList<Long> list = new ArrayList<>();
        addAllCombinationFromDigitsToList(digits, list);
        Collections.sort(list);
        if(list.get(list.size() - 1) == n)
            return -1;
        for (long number : list)
            if(number > n)
                return number;
        return n;
    }
    private static void addAllCombinationFromDigitsToList(int[] digits, ArrayList<Long> list) {
        Set<Integer> allIndexes = new HashSet<>();
        for(int i = 0 ; i < digits.length ; i++)
            allIndexes.add(i);
        addAllCombinationFromDigitsToList(digits, list,"",  new HashSet<>(), allIndexes);
    }
    private static void addAllCombinationFromDigitsToList(int[] digits, ArrayList<Long> list, String prefix,
                                                          Set<Integer> indexes, Set<Integer> allIndexes) {
        if (indexes.size() == digits.length)
            list.add(Long.valueOf(prefix));
        else{
            HashSet<Integer> indexesLeft = new HashSet<>(allIndexes);
            indexesLeft.removeAll(indexes);
            indexesLeft.forEach(i -> {
                HashSet<Integer> newIndexes = new HashSet<>(indexes);
                newIndexes.add(i);
                addAllCombinationFromDigitsToList(digits,  list, prefix + digits[i], newIndexes, allIndexes);
            });
        }
    }
}
