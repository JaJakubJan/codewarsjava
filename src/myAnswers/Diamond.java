package myAnswers;

public class Diamond {
    public static String getDiamondStringFromSize(int n) {
        if(n < 1 || n % 2 == 0)
            return null;
        StringBuilder diamondBuilder = new StringBuilder();
        for(int i = 0 ; i < n / 2 ; i++) {
            diamondBuilder.append(" ".repeat(n / 2 - i))
                    .append("*".repeat( 2*(i + 1) - 1))
                    .append("\n");
        }
        diamondBuilder.append("*".repeat(n)).append("\n");
        for(int i = n / 2 - 1 ; i >= 0 ; i--) {
            diamondBuilder.append(" ".repeat(n / 2 - i))
                    .append("*".repeat( 2*(i + 1) - 1))
                    .append("\n");
        }
        return diamondBuilder.toString();
    }
}
