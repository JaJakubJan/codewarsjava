package myAnswers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EqualSidesOfAnArray {
    public static int findEvenIndex(int[] arr) {
        List<Integer> list = Arrays.stream(arr).boxed().collect(Collectors.toList());
        for(int i = 0 ; i < arr.length ; i++){
            List<Integer> left = list.subList(0, i);
            List<Integer> right = list.subList(i + 1, list.size());
            if(right.stream().mapToInt(Integer::intValue).sum() ==
                    left.stream().mapToInt(Integer::intValue).sum())
                return i;
        }
        return -1;
    }
}
