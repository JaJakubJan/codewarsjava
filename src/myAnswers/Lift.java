package myAnswers;

import java.util.ArrayList;
import java.util.Arrays;

public class Lift {

    public static int[] theLift(final int[][] queues, final int capacity) {
        ArrayList<Integer> positions = new ArrayList<>();
        positions.add(0);
        while(!isLiftJobDone(queues)){
            goUp(queues, capacity, positions);
            goDown(queues, capacity, positions);
        }
        if(positions.get(positions.size() - 1) != 0)
            positions.add(0);
        int[] result = new int[positions.size()];
        for(int i= 0 ; i < positions.size() ; i++)
            result[i] = positions.get(i);
        return result;
    }

    private static void goUp(int[][] queues, int capacity, ArrayList<Integer> positions) {
        int actual = positions.get(positions.size() - 1);
        ArrayList<Integer> personsInside = new ArrayList<>();
        for(int i = actual ; i < queues.length ; i++){
            boolean isSomeoneEnter = false;
            for(int j = 0 ; j < queues[i].length ; j++)
                if(queues[i][j] > i && personsInside.size() <= capacity){
                    personsInside.add(queues[i][j]);
                    queues[i][j] = -1;
                    isSomeoneEnter = true;
                }
            emptyStage(queues, i);
            if((isSomeoneEnter || gettingOf(queues, i, personsInside)) && actual != i)
                positions.add(i);
            actual = isSomebodyWaiting(queues, i) || isSomeoneEnter ? i : actual;
        }
        if(!isLiftJobDone(queues) && positions.get(positions.size() - 1) != actual)
            positions.add(actual);
    }

    private static void goDown(int[][] queues, int capacity, ArrayList<Integer> positions) {
        int actual = positions.get(positions.size() - 1);
        ArrayList<Integer> personsInside = new ArrayList<>();
        for(int i = actual ; i >= 0 ; i--){
            boolean isSomeoneEnter = false;
            for(int j = 0 ; j < queues[i].length ; j++)
                if(queues[i][j] < i && personsInside.size() <= capacity){
                    personsInside.add(queues[i][j]);
                    queues[i][j] = -1;
                    isSomeoneEnter = true;
                }
            emptyStage(queues, i);
            if((isSomeoneEnter || gettingOf(queues, i, personsInside)) && actual != i)
                positions.add(i);
            actual = isSomebodyWaiting(queues, i) || isSomeoneEnter ? i : actual;
        }
        if(!isLiftJobDone(queues) && positions.get(positions.size() - 1) != actual)
            positions.add(actual);
    }

    private static boolean isLiftJobDone(final int[][] queues){
        boolean isDone = true;
        for(int i = 0 ; i < queues.length ; i++)
            for(int j = 0 ; j < queues[i].length ; j++)
                isDone = isDone && queues[i][j] == i;
        return isDone;
    }

    private static boolean isSomebodyWaiting(int[][] queues,  int stage){
        for(int i = 0 ; i < queues[stage].length ; i++)
            if(queues[stage][i] != stage)
                return true;
        return false;
    }

    private static void emptyStage(int[][] queues, int stage){
        int numberOfPersonsOut = 0;
        for(int person : queues[stage])
            if(person == -1)
                numberOfPersonsOut++;
        int[] updatedStage = new int[queues[stage].length - numberOfPersonsOut];
        int j = 0;
        for(int i = 0 ; i < queues[stage].length ; i++)
            if(queues[stage][i] != -1){
                updatedStage[j] = queues[stage][i];
                j++;
            }
        queues[stage] = updatedStage;
    }

    private static boolean gettingOf(int[][] queues, int stage, ArrayList<Integer> personsInside){
        int numberOfPersonsOut = 0;
        for(int person : personsInside)
            if(person == stage)
                numberOfPersonsOut++;
        int[] updatedStage = new int[queues[stage].length + numberOfPersonsOut];
        System.arraycopy(queues[stage], 0, updatedStage, 0, queues[stage].length);
        for(int i = 0 ; i < numberOfPersonsOut ; i++){
            personsInside.remove((Integer) i);
            updatedStage[i + queues[stage].length] = stage;
        }
        queues[stage] = updatedStage;
        return numberOfPersonsOut > 0;
    }

    public static void main(String[] args) {
        final int[][] queues = {
                new int[0], // G
                new int[0], // 1
                new int[]{1,1}, // 2
                new int[0], // 3
                new int[0], // 4
                new int[0], // 5
                new int[0], // 6
        };
        System.out.println(Arrays.toString(theLift(queues, 5)));
    }
}
