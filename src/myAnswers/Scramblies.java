package myAnswers;

import java.util.HashMap;
import java.util.Map;

public class Scramblies {
    public static boolean scramble(String str1, String str2) {
        Map<Character, Integer> fstWordsMap = calculateCharactersQuantityIn(str1);
        Map<Character, Integer> secWordsMap = calculateCharactersQuantityIn(str2);
        for (char c: secWordsMap.keySet())
            if (!fstWordsMap.containsKey(c) ||
                    fstWordsMap.get(c) < secWordsMap.get(c))
                return false;
        return true;
    }
    private static Map<Character, Integer> calculateCharactersQuantityIn(String word){
        Map<Character, Integer> map = new HashMap<>();
        word.chars().mapToObj(c -> (char) c).forEach(c -> {
            if(map.containsKey(c)) map.replace(c, map.get(c) + 1); else map.put(c, 1); });
        return map;
    }
}
