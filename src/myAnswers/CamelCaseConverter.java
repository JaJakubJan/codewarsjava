package myAnswers;

public class CamelCaseConverter {
    public static String toCamelCase(String str){
        String[] words = str.split("[-_]");
        StringBuilder resStr = new StringBuilder(words[0]);
            for(int i = 1 ; i < words.length ; i++)
                resStr.append(capitalize(words[i]));
        return resStr.toString();
    }
    private static String capitalize(String str) {
        if(str == null || str.isEmpty())
            return str;
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
