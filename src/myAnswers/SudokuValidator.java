package myAnswers;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SudokuValidator {
    public static boolean check(int[][] sudoku) {
        for (int i = 0 ; i < sudoku.length ; i++) {
            if(!checkLine(sudoku[i]))
                return false;
            if (!checkLine(new int[]{sudoku[0][i], sudoku[1][i], sudoku[2][i], sudoku[3][i],
                    sudoku[4][i], sudoku[5][i], sudoku[6][i], sudoku[7][i], sudoku[8][i]}))
                return false;
        }
        for (int i = 0 ; i < sudoku.length ; i += 3)
            for (int j = 0 ; j < sudoku.length ; j += 3)
                if (!checkLine(new int[]{sudoku[i][j], sudoku[i+1][j], sudoku[i+2][j], sudoku[i][j+1],
                        sudoku[i+1][j+1], sudoku[i+2][j+1], sudoku[i][j+2], sudoku[i+1][j+2], sudoku[i+2][j+2]}))
                    return false;
        return true;
    }
    private static boolean checkLine(int[] line){
        return Arrays.stream(line)
                .filter(el -> el != 0)
                .boxed()
                .collect(Collectors.toSet())
                .size() == 9;
    }
}
