package myAnswers;

import java.util.ArrayList;
import java.util.List;

public class BalancedParens {
    public static List<String> balancedParens(int n) {
        if(n < 1)
            return List.of("");
        List<String> result = new ArrayList<>();
        addAllParenthesis(new char[2 * n], 0, n, 0, 0, result);
        return result;
    }
    private static void addAllParenthesis(char[] chars, int pos, int n, int open, int close, List<String> list) {
        if(close == n)
            list.add(new String(chars));
        else {
            if(open > close) {
                chars[pos] = ')';
                addAllParenthesis(chars, pos + 1, n, open, close + 1, list);
            }
            if(open < n) {
                chars[pos] = '(';
                addAllParenthesis(chars, pos + 1, n, open + 1, close, list);
            }
        }
    }
    public static void main (String[] args) {
        int n = 3;
        List<String> list = balancedParens(n);
        list.forEach(System.out::println);
    }
}
