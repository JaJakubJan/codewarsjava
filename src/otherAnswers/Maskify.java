package otherAnswers;

import java.util.Arrays;
import java.util.function.Consumer;

public class Maskify {
    public static String maskify(String str) {
        return (str.length() <= 4) ?
                str :
                String.valueOf(doChain(new char[str.length() - 4], chs -> Arrays.fill(chs, '#')))
                        .concat(str.substring(str.length() - 4));
    }

    private static <T> T doChain(T t, Consumer<T> cont) {
        cont.accept(t);
        return t;
    }
}
