package test.myAnswers;

import myAnswers.Diamond;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DiamondTest {
    @Test
    public void testNull() {
        assertNull(Diamond.getDiamondStringFromSize(0));
    }

    @Test
    public void testNegativeInput() {
        assertNull(Diamond.getDiamondStringFromSize(-7));
    }

    @Test
    public void testEvenInput() {
        assertNull(Diamond.getDiamondStringFromSize(6));
    }

    @Test
    public void testDiamond3() {
        StringBuffer expected = new StringBuffer();
        expected.append(" *\n");
        expected.append("***\n");
        expected.append(" *\n");

        assertEquals(expected.toString(), Diamond.getDiamondStringFromSize(3));
    }

    @Test
    public void testDiamond5() {
        StringBuffer expected = new StringBuffer();
        expected.append("  *\n");
        expected.append(" ***\n");
        expected.append("*****\n");
        expected.append(" ***\n");
        expected.append("  *\n");

        assertEquals(expected.toString(), Diamond.getDiamondStringFromSize(5));
    }

    @Test
    public void testDiamond15() {
        StringBuffer expected = new StringBuffer();
        expected.append("       *\n");
        expected.append("      ***\n");
        expected.append("     *****\n");
        expected.append("    *******\n");
        expected.append("   *********\n");
        expected.append("  ***********\n");
        expected.append(" *************\n");
        expected.append("***************\n");
        expected.append(" *************\n");
        expected.append("  ***********\n");
        expected.append("   *********\n");
        expected.append("    *******\n");
        expected.append("     *****\n");
        expected.append("      ***\n");
        expected.append("       *\n");

        assertEquals(expected.toString(), Diamond.getDiamondStringFromSize(15));
    }

}