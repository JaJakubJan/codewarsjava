package test.myAnswers;

import myAnswers.NextBiggerNumberFinder;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class NextBiggerNumberFinderTest {
    @Test
    public void basicTests() {
        assertEquals(21, NextBiggerNumberFinder.nextBiggerNumber(12));
        assertEquals(531, NextBiggerNumberFinder.nextBiggerNumber(513));
        assertEquals(2071, NextBiggerNumberFinder.nextBiggerNumber(2017));
        assertEquals(441, NextBiggerNumberFinder.nextBiggerNumber(414));
        assertEquals(414, NextBiggerNumberFinder.nextBiggerNumber(144));
    }
}
