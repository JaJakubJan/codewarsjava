package test.myAnswers;

import myAnswers.Calculator;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    public static Random rand = new Random();

    @Test
    public void simpleLiteral() {
        Double d = new Double(rand.nextInt());
        assertEquals( d, Calculator.evaluate(d.toString()));
    }

    @Test
    public void subtractionAndAddition() {
        Double a = new Double(rand.nextInt());
        Double b = new Double(rand.nextInt());
        Double c = new Double(rand.nextInt());
        assertEquals(new Double(a+b), Calculator.evaluate(a.intValue() + " + " + b.intValue()));
        assertEquals(new Double(a-b-c), Calculator.evaluate(a.intValue() + " - " + b.intValue() + " - " + c.intValue()));
    }

    @Test
    public void divisionAndMultiplication() {
        assertEquals(new Double(25), Calculator.evaluate("10 * 5 / 2"), 1e-4);
    }

    @Test
    public void allMixed() {
        assertEquals(new Double(7), Calculator.evaluate("2 / 2 + 3 * 4 - 6"), 1e-4);
        assertEquals(new Double(8), Calculator.evaluate("2 + 3 * 4 / 3 - 6 / 3 * 3 + 8"), 1e-4);
    }

    @Test
    public void floats() {
        assertEquals(new Double(6.6), Calculator.evaluate("1.1 + 2.2 + 3.3"), 1e-4);
        assertEquals(new Double(7.986), Calculator.evaluate("1.1 * 2.2 * 3.3"), 1e-4);
    }
}