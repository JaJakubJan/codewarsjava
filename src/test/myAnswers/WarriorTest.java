package test.myAnswers; 

import myAnswers.Warrior;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorTest {

    @Test public void sampleTest1() {

        Warrior tom = new Warrior();
        assertEquals(1,          tom.level());
        assertEquals(100,        tom.experience());
        assertEquals("Pushover", tom.rank());
    }

    @Test public void sampleTest2() {

        Warrior bruce_lee = new Warrior();

        assertEquals(1,                                      bruce_lee.level());
        assertEquals(100,                                    bruce_lee.experience());
        assertEquals("Pushover",                             bruce_lee.rank());
        assertEquals(new ArrayList<String>(),                bruce_lee.achievements());
        assertEquals("Defeated Chuck Norris",                bruce_lee.training("Defeated Chuck Norris", 9000, 1));
        assertEquals(9100,                                   bruce_lee.experience());
        assertEquals(91,                                     bruce_lee.level());
        assertEquals("Master",                               bruce_lee.rank());
        assertEquals("A good fight",                         bruce_lee.battle(90));
        assertEquals(9105,                                   bruce_lee.experience());
        assertEquals(Arrays.asList("Defeated Chuck Norris"), bruce_lee.achievements());
    }


    private void check(int act, int exp, String msg)                   { assertEquals(exp, act); }
    private void check(String act, String exp, String msg)             { assertEquals(exp, act); }
    private void check(List<String> act, List<String> exp, String msg) { assertEquals(exp, act); }



    @Test
    public void fixedTests() {
        Warrior goku = new Warrior();

        check(goku.level(), 1, "All warriors must start as LV 1");
        check(goku.rank(), "Pushover", "All warriors must start as a Pushover");
        check(goku.achievements(), Arrays.asList(), "All warriors must start off with no achievements to their name");
        check(goku.training("Do ten push-ups", 95, 1), "Do ten push-ups", "After obtaining a mastery, the achievement must be listed and returned");
        check(goku.level(), 1, "After your warriors's first training, he/she must still be LV 1");
        // additional test for invalid level;
        check(goku.battle(0), "Invalid level", "LV 0 enemies do not exist. This must be a mistake!");
        check(goku.battle(1), "A good fight", "After fighting against a warrior of equal level, your warrior should provide the appropiate response");
        check(goku.level(), 2, "With that fight under your belt, your warrior should now be ascended to LV 2");
        check(goku.achievements(), Arrays.asList("Do ten push-ups"), "Don't forget what your warrior has achieved so far");
        check(goku.rank(), "Pushover", "A pushover, however, your warrior should remain");
        check(goku.battle(3), "An intense fight", "Another battle, this time with a slightly more skilled enemy, should have your warrior provide an appropiate response");
        check(goku.level(), 2, "Your should still remain LV 2");
        check(goku.training("Survive one night at the Forest of Death", 170, 2), "Survive one night at the Forest of Death", "After obatining a mastery, your achievement must be listed and returned");
        check(goku.training("Mastered the Spirit Bomb", 1580, 10), "Not strong enough", "Your warrior isn't skilled enough to do everything just yet");
        check(goku.achievements(), Arrays.asList("Do ten push-ups", "Survive one night at the Forest of Death"), "Your warrior shouldn't forget his achievements");
        check(goku.battle(2), "A good fight", "Your enemy isn't exactly your level. Provide the appropriate response");
        check(goku.level(), 4, "Your warrior should now be LV 4");
        check(goku.experience(), 400, "Your warrior's experience should be relative to his level");
        check(goku.battle(9), "An intense fight", "Your warrior picked a fight with a relatively strong enemy. Nothing he can't hold his/er own to. Provide the appropiate response");
        check(goku.battle(14), "You've been defeated", "Your warrior picked a fight he/she can't win.");
        check(goku.level(), 9, "Your level should be high than our last viewing, regardless of our recent defeat");
        check(goku.battle(12), "An intense fight", "Your enemy's in a whole other league, but he's still within your reach");
        check(goku.battle(8), "Easy fight", "Going back to the small fry, huh? Provide the appropiate respone") ;
        check(goku.rank(), "Novice", "You definately ain't no pushover anymore!");
        check(goku.experience(), 1080, "Startin' to get some serious experience points?");
        check(goku.battle(140), "Invalid level", "Something not right here...");
        check(goku.training("Mastered the Spirit Bomb", 1580, 10), "Mastered the Spirit Bomb", "Remember that training you weren't strong enough to do before? What's different now?");
        check(goku.level(), 26, "Finally got some decent skill in you");
        check(goku.rank(), "Fighter", "You ain't in the beginner tiers anymore!");
        check(goku.battle(30), "An intense fight", "The enemy's strong, but you can hold your own");
        check(goku.rank(), "Fighter", "Still a fighter at heart");
        check(goku.level(), 29, "Your ascention nears...");
        check(goku.experience(), 2980, "...ever so closely");
        check(goku.training("Mastered the Shadow Clone Jutsu", 19, 6), "Mastered the Shadow Clone Jutsu", "Another technique doesn't hurt");
        check(goku.experience(), 2999, "...sooooo very close");
        check(goku.level(), 29, "But no cigar");
        check(goku.battle(32), "An intense fight", "Gotta get your gain right. What kinda fight helps with that?");
        check(goku.rank(), "Warrior", "You ain't the greatest, but you still a...");
        check(goku.experience(), 3179, "Keep rackin' the experience");
        check(goku.battle(39), "An intense fight", "Keep the big fights poppin'");
        check(goku.rank(), "Veteran", "I think you're onto something with this whole fighting thing");
        check(goku.experience(), 4459, "Think you know your way around a fight now?");
        check(goku.level(), 44, "...Well, do you");
        check(goku.training("Defeat Superman", 10000, 100), "Not strong enough", "Don't jump the gun just yet!");
        check(goku.training("Mastered the Spirit Gun", 1340, 43), "Mastered the Spirit Gun", "That's more up your alley");
        check(goku.rank(), "Sage", "What becomes of veterans when they start to become old?");
        check(goku.experience(), 5799, "Quite the fighter now, aren't ya?");
        check(goku.level(), 57, "Keep rising!");
        check(goku.battle(61), "An intense fight", "Nothin' a Sage can't handle!");
        check(goku.rank(), "Elite", "You're very good at what you do");
        check(goku.experience(), 6119, "Experience needs to match the title");
        check(goku.training("Mastered the Perfect Roundhouse Kick", 1781, 60), "Mastered the Perfect Roundhouse Kick", "Patrick Swayze would be proud");
        check(goku.rank(), "Conqueror", "You're becoming a legend, yo!");
        check(goku.experience(), 7900, "Ten lifetimes' worth of experience right here!");
        check(goku.battle(83), "An intense fight", "A conqueror's gonna conquer, amirite?");
        check(goku.level(), 82, "This is a level to be feared");
        check(goku.rank(), "Champion", "The world looks to you for your skills now");
        check(goku.experience(), 8220, "You're geeting too good that this fighting stuff");
        check(goku.training("Defeat The Four Horsemen", 1100, 82), "Defeat The Four Horsemen", "Can the judgement of the Gods stop you anymore?");
        check(goku.battle(100), "You've been defeated", "Your power is godlike, but there is still greater");
        check(goku.rank(), "Master", "Your competition is quickly strinking");
        check(goku.level(), 93, "Your greatness is rarely matched");
        check(goku.experience(), 9320, "Your power is rising...overflowing!");
        check(goku.training("Win the Intergalaxtic Tournament", 679, 91), "Win the Intergalaxtic Tournament", "Can you prove to be the Master of Masters?");
        check(goku.experience(), 9999, "Only one stands in your way now...");
        check(goku.training("Fight Superman to a draw", 11000, 99), "Fight Superman to a draw", "If you can do this... I think you might be the one");
        // additional test for invalid level;
        check(goku.battle(999), "Invalid level", "No, not even Superman can have more than 100 levels!");
        check(goku.level(), 100, "..Wait, can it be?");
        check(goku.experience(), 10000, "..Is it really you?");
        check(goku.rank(), "Greatest", "Yes, you truly are the greatest!!! ... as soon as you pass this test at least");
        check(goku.training("Defeat Superman", 10000, 100), "Defeat Superman", "*creates shrine in your honor*");
        check(goku.achievements(), Arrays.asList("Do ten push-ups", "Survive one night at the Forest of Death", "Mastered the Spirit Bomb", "Mastered the Shadow Clone Jutsu", "Mastered the Spirit Gun", "Mastered the Perfect Roundhouse Kick", "Defeat The Four Horsemen", "Win the Intergalaxtic Tournament", "Fight Superman to a draw", "Defeat Superman" ), "What a career you have!");
    }



    /* ******************
     *    RANDOM TESTS
     * ******************/

    final private static class RefWarriorStuff {

        final static private int      MAX_LVL        = 100,
                MAX_XPS        = 10000,
                LVL_XP_RATIO   = 100;

        final static private String[] RANKS          = {"", "Pushover", "Novice", "Fighter", "Warrior", "Veteran", "Sage", "Elite", "Conqueror", "Champion", "Master", "Greatest"},
                STR_BATTLE     = {"Easy fight", "A good fight", "An intense fight"};

        final static private String   DEF_RET_ACHIEV = "Not strong enough",
                INVALID_BATTLE = "Invalid level",
                FAILED_BATTLE  = "You've been defeated";


        private int          xps    = LVL_XP_RATIO;
        private List<String> achiev = new ArrayList<>();


        public int          experience()   { return xps; }
        public int          level()        { return xps / LVL_XP_RATIO; }
        public String       rank()         { return RANKS[ getRank(xps) ]; }
        public List<String> achievements() { return achiev.stream().collect(Collectors.toList()); }

        private int  getRank(int xp)       { return xp/1000 + 1; }
        private void updateXps(int xp)     { xps = Math.min(xps+xp, MAX_XPS); }


        public String battle(int oLvl) {

            if (oLvl<1 || oLvl > MAX_LVL)
                return INVALID_BATTLE;

            int diff = oLvl - level();
            if (diff >= 5 && getRank(xps) < getRank(oLvl*LVL_XP_RATIO))
                return FAILED_BATTLE;

            int xpGain = diff>0 ? 20 * diff * diff
                    : Math.max(0, 10 + 5*diff),
                    iRet   = diff==-1 ? 1
                            : diff < 0 ? 0
                            : diff > 0 ? 2 : 1;

            updateXps(xpGain);
            return STR_BATTLE[iRet];
        }

        public String training(String ach, int xpGain, int minLvl) {
            if (level() < minLvl) return DEF_RET_ACHIEV;
            updateXps(xpGain);
            achiev.add(ach);
            return ach;
        }
    }



    @Test public void randomTests() {

        Warrior user = null;
        RefWarriorStuff ref = null;


        for (int i=0 ; i<200 ; i++) {

            if (i%20==0) {

                System.out.println("\n\n**************************\n  CREATING A NEW WARRIOR  \n**************************");
                user = new Warrior();
                ref  = new RefWarriorStuff();

            } else if (Math.random() < .5) {

                int    enemyLvl = ref.level()-5 + (int) (Math.random()*16);
                String exp      = ref.battle(enemyLvl);

                System.out.println(String.format("Fighting a level %s enemy, expecting: \"%s\"", enemyLvl, exp));
                assertEquals(exp, user.battle(enemyLvl));

            } else {

                String ach = "Do the Hookie Pookie";
                int    a   = 100 + (int) (Math.random() * 901),
                        b   = 1   + (int) (Math.random() * Math.min(25, ref.level()+5) );
                String exp = ref.training(ach, a, b);

                System.out.println(String.format("Doing a level %s training for %s exp, expecting: \"%s\"", b, a, exp));
                assertEquals(exp, user.training(ach, a, b));
            }

            System.out.println(String.format("Checking all stats, expecting: %s exp, level %s", ref.experience(), ref.level()));
            assertEquals(ref.level(), user.level());
            assertEquals(ref.rank(), user.rank());
            assertEquals(ref.experience(), user.experience());
            assertEquals(ref.achievements(), user.achievements());

            System.out.println("");
        }
    }
}
