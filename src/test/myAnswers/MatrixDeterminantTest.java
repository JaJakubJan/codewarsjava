package test.myAnswers;

import myAnswers.MatrixDeterminant;
import org.junit.jupiter.api.Test;
import java.util.Random;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MatrixDeterminantTest {
    private static int[][][] matrix = { {{1}},
            {{1, 3}, {2,5}},
            {{2,5,3}, {1,-2,-1}, {1, 3, 4}}};

    private static int[] expected = {1, -1, -20};

    @Test
    public void sampleTests() {
        for (int n = 0 ; n < expected.length ; n++)
            assertEquals(expected[n], MatrixDeterminant.determinant(matrix[n]));
    }


    private static int[][][] matrix2 = { {{5}},
            {{4, 6}, {3,8}},
            {{2,4,2},{3,1,1},{1,2,0}},
            {{6,1,1}, {4,-2,5}, {2, 8, 7}},
            {{2,4,-3}, {1,8,7}, {2, 3, 5}},
            {{1, 2, 3, 4}, {5, 0, 2, 8}, {3,5,6,7},{2,5,3,1}},
            {{2,5,3,6,3}, {17,5,7,4,2}, {7,8,5,3,2}, {9,4,-6,8,3}, {2,-5,7,4,2}},
            {{1, 2, 4, 0, 9},{2, 3, 4, 1, 1},{6, 7, 3, 9, 3},{2, 0, 3, 0, 2},{4, 5, 2, 3, 1}},
            {{2, 4, 5, 3, 1, 2},{2, 4, 7, 5, 3, 2},{1, 1, 0, 2, 3, 1},{1, 3, 9, 0, 3, 2},{1, 1, 2, 2, 4, 1},{0, 0, 4, 1, 2, 3}},
            {{3, 2, 1, 4, 0, 1},{1, 2, 3, 1, 9, 1},{0, 2, 1, 1, 9, 0},{8, 2, 1, 0, 2, 3},{2, 3, 4, 0, 1, 2},{2, 1, 0, 0, 1, 1}}};

    private static int[] expected2 = {5, 14, 10, -306, 113, 24, 2060, 1328, 88, -536};

    @Test
    public void moreTests() {
        for (int n = 0 ; n < expected2.length ; n++)
            assertEquals(expected2[n], MatrixDeterminant.determinant(matrix2[n]));
    }


    @Test
    public void randomTests() {
        for (int n = 0 ; n < 50 ; n++) {
            int mS = 1 + rand.nextInt(10);
            int[][] rMat = new int[mS][mS];
            for (int x = 0 ; x < mS ; x++) for (int y = 0 ; y < mS ; y++) {
                rMat[x][y] = rand.nextInt(20) - 10;
            }
            int expected = SolMat101.determinant(rMat);
            assertEquals(expected, MatrixDeterminant.determinant(rMat));
        }
    }


    private Random rand = new Random();

    private static class SolMat101 {

        public static int determinant(int[][] m) {
            int d = 0, size = m.length;
            if (size == 1) return m[0][0];

            for (int n = 0 ; n < size ; n++) {
                int[][] newM = new int[size-1][size-1];
                for (int x = 1 ; x < size ; x++) for (int y = 0 ; y < size ; y++) {
                    if (y == n) continue;
                    newM[x-1][y + (y>n ? -1 : 0)] = m[x][y];
                }
                d += (n%2!=0 ? -1 : 1) * m[0][n] * determinant(newM);
            }
            return d;
        }
    }
}

